# Paragraphs content

## Modules

* `paragraphs_content` :: basic structure: media, filters, text editors, paragraphs
* `paragraphs_content_types` :: page and blog content types

## Dependencies

* Paragraphs
* Media

## Structure

* Paragraph types: image, text
* Media types: files, image
